package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {
	//データを取得する部分
    public List<UserMessage> select(Connection connection, Integer id, int num, String start, String end) {

    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("SELECT ");
    		sql.append("    messages.id as id, ");
    		sql.append("    messages.text as text, ");
    		sql.append("    messages.user_id as user_id, ");
    		sql.append("    users.account as account, ");
    		sql.append("    users.name as name, ");
    		sql.append("    messages.created_date as created_date ");
    		sql.append("FROM messages ");
    		sql.append("INNER JOIN users ");
    		sql.append("ON messages.user_id = users.id ");
    		sql.append("WHERE messages.created_date ");
    		sql.append("BETWEEN");
    		sql.append('?');
    		sql.append("AND");
    		sql.append('?');

    		if(id != null) {
    			sql.append("AND user_id = ? ");
    		}
    		sql.append("ORDER BY created_date DESC limit " + num);

    		ps = connection.prepareStatement(sql.toString());

    		ps.setString(1, start);
    		ps.setString(2, end);

    		if(id != null) {
    			ps.setInt(3, id);
    		}

    		ResultSet rs = ps.executeQuery();

    		List<UserMessage> messages = toUserMessages(rs);
    		return messages;

    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }
//    ResultSetでは情報が取得しづらい。
//    取得したデータを自分で定義したUserMessage型にきれいにtoUserMessagesで整形
    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
        	//「ポインター」といい、エクセルの表みたいに一行ずつ読むのがずれていく
            while (rs.next()) {
                UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));
                message.setText(rs.getString("text"));
                message.setUserId(rs.getInt("user_id"));
                message.setAccount(rs.getString("account"));
                message.setName(rs.getString("name"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}